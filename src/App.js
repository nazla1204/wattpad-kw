import React from 'react';
import {
  Navbar,
  Container,
  Badge,
  h1,
  Nav,
  NavDropdown,
  Form,
  FormControl,
  Button,
  Card,
  Row,
  Col
}from 'react-bootstrap';

function Header() {
  return(
      <Navbar bg="light">
    <Navbar.Brand href="#home">
      <img
        alt=""
        src="https://www.wattpad.com/img//logos/wp-logo-orange.png"
        width="123"
        height="27"
        className="d-inline-block align-top"
      />
    </Navbar.Brand>
    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="mr-auto">
      <NavDropdown title="Jelajahi" id="collasible-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
      </NavDropdown>
      <NavDropdown title="Komunitas" id="collasible-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
      </NavDropdown>
    </Nav>
  </Navbar.Collapse>
  <Form inline>
      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
      <Button variant="outline-light">Search</Button>
    </Form>
    <Nav className="mr-auto">
      <Nav.Link href="#login">Masuk</Nav.Link>
      </Nav>
      <NavDropdown title="Bahasa Indonesia" id="collasible-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
      </NavDropdown>

  </Navbar>
    );
}

function Main() {
  return(
    <Container>
      <br></br>
      <br></br>
      <br></br>
    <br></br>
    <br></br><br></br>
      <Row>
        <Col>
        <h1>Hai ini, Wattpad.</h1>
        <br></br>
    <h5>Platform sosial untuk bercerita yang paling disukai di dunia</h5>
    <br></br>
    <h6>Wattpad menghubungkan komunitas global dari 90 juta pembaca dan penulis melalui kekuatan cerita.</h6>
    <br></br>
    <Button variant="danger">Mulai Membaca</Button>{' '}
    <Button variant="danger">Mulai Menulis</Button>{' '}
    </Col>
        <Col>
        <img src='https://www.wattpad.com/img/landing/hero-devices.png' width='470px' height='297px'></img>
        </Col>
    </Row>
    <br></br>
    <br></br>
    <br></br><br></br>
    <br></br>
    <br></br>
    <br></br><br></br>
    <h5 style={{textAlign:"center"}}>Lihat Ceritamu...</h5>
    <Row>
      <Col style={{textAlign:"center"}}>
      <img src="https://www.wattpad.com/img//landing/tv-icon.svg"></img>
      <h6>Diproduksi menjadi film</h6>
      </Col>
      <Col style={{textAlign:"center"}}>
      <img src="https://www.wattpad.com/img//landing/reel-icon.svg"></img>
      <h6>Diadaptasi menjadi serial TV</h6>
      </Col>
      <Col style={{textAlign:"center"}}>
      <img src="https://www.wattpad.com/img//landing/book-icon.svg"></img>
      <h6>Diterbitkan</h6>
      </Col>
    </Row>
    <br></br>
    <br></br>
    <br></br>
    <br></br><br></br>
    <br></br>
    <br></br>
    <br></br><br></br>
    <Row>
      <Col>
      <img src="https://www.wattpad.com/img/landing/hero-devices.png" width="470px" height='297px'></img>
      </Col>
      <Col>
      <img src="https://www.wattpad.com/img//landing/wattpadStudiosLogo.svg"></img>
      <h5>Cerita orisinal kamu dapat menjadi sangat sukses nantinya</h5>
      <p>Wattpad Studios menemukan penulis-penulis yang tak dikenal, belum dikontrak,
         tetapi berbakat di Wattpad dan menghubungkan mereka dengan perusahaan hiburan multimedia global.</p>
         <br></br>
      <p>Wattpad Studios bekerja dengan mitra seperti:</p>
      <br></br>
      <Col>
      <Row>
      <Col><img src="https://www.wattpad.com/img//landing/sony.svg"></img></Col>
      <Col><img src="https://www.wattpad.com/img//landing/hulu.svg"></img></Col>
      <Col><img src="https://www.wattpad.com/img//landing/syfy.svg"></img></Col>
      </Row>
      </Col>
      </Col>
    </Row>
    <br></br>
    <br></br>
    <br></br>
    <br></br>
    <br></br><br></br>
    <Row>
      <Col style={{textAlign:"right"}}>
      <img src="https://www.wattpad.com/img//landing/wattpadBooksLogo.svg"></img>
      <h5>Ceritamu akan menjadi komponen istimewa di rak buku</h5>
      <p>Wattpad Books bercita-cita untuk mengenali dan mewakili suara-suara yang
        beragam dengan mengubah cerita Wattpad menjadi buku yang diterbitkan di seluruh dunia.</p>
         <br></br>
      <p>Wattpad Studios bekerja dengan mitra seperti:</p>
      <Row>
      <Col><img src="https://www.wattpad.com/img//landing/macmillan.png" style={{width:"75px"}}></img></Col>
      <Col><img src="https://www.wattpad.com/img//landing/anvil.png" style={{width:"75px"}}></img></Col>
      <Col><img src="https://www.wattpad.com/img//landing/penguin.png" style={{width:"75px"}}></img></Col>
      </Row>
      </Col>
<br></br>
      <Col>
      <img src="https://www.wattpad.com/img//landing/wattpadBooksPoster.png" width="348px" height="315px"></img>
      </Col>
    </Row>
    </Container>
  );
}

function App() {
  return (
    <div>
      <Header/>
      <Main/>
    </div>
  );
}

export default App;
